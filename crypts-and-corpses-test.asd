;;;; crypts-and-corpses-test.asd

(asdf:defsystem #:crypts-and-corpses-test
  :author "Lily Carpenter <lily-license@azrazalea.net"
  :license "AGPLv3"
  :depends-on (:crypts-and-corpses
               :prove
               :check-it)
  :pathname "src/test"
  :components ((:file "suite"))
  :description "Testing Crypts and Corpses")
