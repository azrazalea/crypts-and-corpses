(defpackage :crypts-and-corpses-test
  (:use #:cl
        #:prove
        #:check-it))

(in-package :crypts-and-corpses-test)

(defun run-tests ()
  (plan nil)
  (subtest :game-map
    (fail "Not implemented"))
  (subtest :entity
    (fail "Not Implemented"))
  (subtest :tile-entity
    (fail "Not Implemented"))
  (subtest :viewport
    (fail "Not Implemented"))
  (subtest :mobile
    (fail "Not Implemented"))
  (subtest :player-character
    (fail "Not Implemented"))
  (subtest :calc-coord
    (fail "Not Implemented"))
  (subtest :parse-window-size
    (fail "Not Implemented"))
  (subtest :out-of-bounds-p
    (fail "Not Implemented"))
  (subtest :overrun-p
    (fail "Not Implemented"))
  (subtest :calculated-effective-location
    (fail "Not Implemented"))
  (subtest :bad-move-p
    (fail "Not Implemented"))
  (subtest :handle-input
    (fail "Not Implemented"))
  (subtest :create-viewport
    (fail "Not Implemented"))
  (finalize))
