;;;; crypts-and-corpses.asd

(asdf:defsystem #:crypts-and-corpses
  :description "A necromancer themed roguelike."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :depends-on ("bearlibterminal"
               "split-sequence"
               "crawler"
               "alexandria"
               "cl-heap")
  :serial t
  :pathname "src"
  :components ((:file "package")
               (:file "internal")))
