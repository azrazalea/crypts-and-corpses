;;;; package.lisp

(defpackage #:crypts-and-corpses
  (:use)
  (:export #:start))

(defpackage #:crypts-and-corpses-internal
  (:use #:cl
        #:bearlibterminal
        #:split-sequence
        #:crypts-and-corpses))
