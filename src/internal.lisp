;;;; Copyright (C) 2016 Lily Carpenter

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;; crypts-and-corpses.lisp

(in-package #:crypts-and-corpses-internal)

(declaim (notinline cl-heap:dequeue cl-heap:enqueue))

(defstruct game-map
  (width  99 :type fixnum :read-only t)
  (height 39 :type fixnum :read-only t)
  (dungeon nil :read-only t)
  (mobiles (make-hash-table) :type hash-table :read-only t)
  (terrain (make-hash-table) :type hash-table :read-only t))

(defmethod initialize-instance :after ((map game-map) &rest initargs)
  (declare (ignore initargs))
  (with-slots (width height dungeon) map
    (setf dungeon (crawler:make-dungeon width height))))

(defclass entity ()
  ((width :initarg :width :type fixnum :initform 1)
   (height :initarg :height :type fixnum :initform 1)
   (location :initarg :location :initform 0 :type fixnum :reader get-location)
   (color :initarg :color :initform (color-from-name "#ffffff"))
   (blocks-light :initform nil :reader get-blocks-light)))

(defclass tile-entity (entity)
  ((codepoint :initarg :codepoint :initform #x3f :type fixnum)))

(defclass wall (tile-entity)
  ((codepoint :initarg :codepoint :initform #x23 :type fixnum)
   (color :initarg :color :initform (color-from-name "#999999"))
   (blocks-light :initform t)))

(defclass floor-tile (tile-entity)
  ((codepoint :initarg :codepoint :initform #xb7 :type fixnum)
   (color :initarg :color :initform (color-from-name "#3f3f3f"))
   (player-map-value :initform nil :reader get-player-map-value)
   (enemy-map-value :initform nil :reader get-enemy-map-value)))

(defclass viewport (entity)
  ((width :initarg :width :type fixnum :initform 99)
   (height :initarg :height :type fixnum :initform 40)
   (location :initarg :location :type fixnum :initform 0)))

(defclass mobile (tile-entity)
  ((health :initarg :health :type fixnum :initform 10)
   (mana :initarg :mana :type fixnum :initform 0)
   (max-health :initarg :health :type fixnum :initform 10)
   (max-mana :initarg :mana :type fixnum :initform 10)
   (health-regen :initarg :health-regen :initform 0 :type fixnum)
   (mana-regen :initarg :mana-regen :initform 0 :type fixnum)
   (vision-range :initform 4 :initarg :vision-rage :type fixnum :reader get-vision-range)))

(defclass player-character (mobile)
  ((health :initarg :health :type fixnum :initform 20)
   (mana :initarg :mana :type fixnum :initform 10)
   (max-health :initarg :health :type fixnum :initform 20)
   (codepoint :initform #x2639)
   (ranged-attack-damage :initarg :ranged-attack-damage :initform 5 :type fixnum)
   (melee-attack-damage :initarg :ranged-attack-damage :initform 2 :type fixnum)
   (ranged-attack-cost :initarg :ranged-attack-cost :initform 5 :type fixnum)
   (mana-regen :initarg :mana-regen :initform 3 :type fixnum)
   (color :initarg :color :initform (color-from-name "#800080"))))

(defclass enemy (mobile)
  ((codepoint :initform #x263a)
   (melee-attack-damage :initarg :melee-attack-damage :initform 1 :type fixnum)
   (health-regen :initarg :health-regen :initform 0 :type fixnum)
   (color :initarg :color :initform (color-from-name "#1919ff"))))

(defclass tomb (floor-tile)
  ((codepoint :initform #x2616)
   (color :initform (color-from-name "#ffff00"))
   (awakened :initform nil :reader awakened-p :accessor awakened)))

(defclass minion (mobile)
  ((color :initarg :color :initform (color-from-name "#ffff00"))
   (codepoint :initform #x2639)
   (melee-attack-damage :initform 2 :type fixnum )))

(defun calc-coord (location map)
  (when location
    (values (mod location (game-map-width map)) (floor (/ location (game-map-width map))))))

(defun get-window-size ()
  (apply 'values (mapcar #'parse-integer (split-sequence #\x (terminal-get "window.size" "0x0")))))

(defun calc-location (x y map)
  (when x y map
        (+ (* (game-map-width map) y) x)))

(defgeneric set-location (object x y map))
(defmethod set-location ((object tile-entity) x y map)
  (setf (slot-value object 'location) (calc-location x y map)))

(defgeneric end-of-turn (object))

(defmethod end-of-turn ((object mobile))
  (incf (slot-value object 'health) (slot-value object 'health-regen))
  (incf (slot-value object 'mana) (slot-value object 'mana-regen))
  (when (> (slot-value object 'health) (slot-value object 'max-health))
    (setf (slot-value object 'health) (slot-value object 'max-health)))
  (when (> (slot-value object 'mana) (slot-value object 'max-mana))
    (setf (slot-value object 'mana) (slot-value object 'max-mana))))

(defgeneric move-left (object map))
(defgeneric move-right (object map))
(defgeneric move-up (object map))
(defgeneric move-down (object map))

(defgeneric update-location (object map old-location new-location))

(defun out-of-bounds-p (new-location map)
  (multiple-value-bind (x y) (calc-coord new-location map)
    (or (< x 0)
        (< y 0)
        (>= x (game-map-width map))
        (>= y (game-map-height map)))))

(defun overrun-p (old-location new-location map)
  (multiple-value-bind (old-x old-y) (calc-coord old-location map)
    (multiple-value-bind (new-x new-y) (calc-coord new-location map)
      (or (and (not (= old-y new-y))
               (not (= old-x new-x)))))))

(defun calculate-effective-location (object map location)
  (+
   location
   (1- (slot-value object 'width))
   (* (1- (slot-value object 'height)) (game-map-width map))))

(defun collision-p (new-location map)
  (let ((thing (gethash new-location (game-map-mobiles map)))
        (tile (gethash new-location (game-map-terrain map))))
    (or thing
        (and tile (not (typep tile 'floor-tile))))))

(defun bad-move-p (object map old-location new-location)
  (when old-location new-location object
    (let ((old-effective-location (calculate-effective-location object map old-location))
          (new-effective-location (calculate-effective-location object map new-location)))
      (or (out-of-bounds-p new-effective-location map)
          (collision-p new-effective-location map)
          (overrun-p old-effective-location new-effective-location map)))))

(defmethod update-location ((object entity) map old-location new-location)
  (unless (bad-move-p object map old-location new-location)
    (setf (slot-value object 'location) new-location)))

(defmethod update-location ((object mobile) map old-location new-location)
  (unless (bad-move-p object map old-location new-location)
    (setf (slot-value object 'location) new-location)
    (setf (gethash new-location (game-map-mobiles map)) object)
    (remhash old-location (game-map-mobiles map))))

(defmethod update-location :after ((player player-character) map old-location new-location)
  (declare (ignore old-location new-location))
  (make-dijkstra-map (list player) 'player-map-value (game-map-terrain map) map))

(defmethod update-location :after ((enemy enemy) map old-location new-location)
  (declare (ignore old-location new-location))
  (make-dijkstra-map
   (remove-if-not #'enemy-p
                  (alexandria:hash-table-values (game-map-mobiles map)))
   'enemy-map-value (game-map-terrain map) map))

(defmethod move-left ((object entity) map)
  (let ((old-location (slot-value object 'location))
        (new-location (1- (slot-value object 'location))))
    (update-location object map old-location new-location)))

(defmethod move-up ((object entity) map)
  (let ((old-location (slot-value object 'location))
        (new-location (- (slot-value object 'location) (game-map-width map))))
    (update-location object map old-location new-location)))

(defmethod move-down ((object entity) map)
  (let ((old-location (slot-value object 'location))
        (new-location (+ (slot-value object 'location) (game-map-width map))))
    (update-location object map old-location new-location)))

(defmethod move-right ((object entity) map)
  (let ((old-location (slot-value object 'location))
        (new-location (1+ (slot-value object 'location))))
    (update-location object map old-location new-location)))

(defun find-mobile-left (location map)
  (let ((found-object nil))
    (loop for i from (1- location) downto 0 do
      (when (typep (gethash i (game-map-terrain map)) 'wall)
        (return nil))
      (setf found-object (gethash i (game-map-mobiles map)))
      (when (not (typep found-object 'mobile))
        (setf found-object nil))
          until found-object)
    found-object))

(defun find-mobile-right (location map)
  (let ((found-object nil))
    (loop for i from (1+ location) to (* (game-map-width map) (game-map-height map)) do
      (when (typep (gethash i (game-map-terrain map)) 'wall)
        (return nil))
      (setf found-object (gethash i (game-map-mobiles map)))
      (when (not (typep found-object 'mobile))
        (setf found-object nil))
          until found-object)
    found-object))

(defun find-mobile-up (location map)
  (let ((found-object nil))
    (loop for i from (- location (game-map-width map)) downto 0 by (game-map-width map) do
      (when (typep (gethash i (game-map-terrain map)) 'wall)
        (return nil))
      (setf found-object (gethash i (game-map-mobiles map)))
      (when (not (typep found-object 'mobile))
        (setf found-object nil))
          until found-object)
    found-object))

(defun find-mobile-down (location map)
  (let ((found-object nil))
    (loop for i from (+ location (game-map-width map)) to (* (game-map-width map) (game-map-height map)) by (game-map-width map) do
      (when (typep (gethash i (game-map-terrain map)) 'wall)
        (return nil))
      (setf found-object (gethash i (game-map-mobiles map)))
      (when (not (typep found-object 'mobile))
        (setf found-object nil))
          until found-object)
    found-object))

(defgeneric shoot-left (object map))
(defgeneric shoot-right (object map))
(defgeneric shoot-up (object map))
(defgeneric shoot-down (object map))

(defun shoot (player mobile)
  (when (and mobile (>= (slot-value player 'mana) (slot-value player 'ranged-attack-cost)))
    (decf (slot-value mobile 'health) (slot-value player 'ranged-attack-damage))
    (decf (slot-value player 'mana) (slot-value player 'ranged-attack-cost))))

(defmethod shoot-left ((player player-character) map)
  (let ((target (find-mobile-left (slot-value player 'location) map)))
    (shoot player target)))

(defmethod shoot-right ((player player-character) map)
  (let ((target (find-mobile-right (slot-value player 'location) map)))
    (shoot player target)))

(defmethod shoot-up ((player player-character) map)
  (let ((target (find-mobile-up (slot-value player 'location) map)))
    (shoot player target)))

(defmethod shoot-down ((player player-character) map)
  (let ((target (find-mobile-down (slot-value player 'location) map)))
    (shoot player target)))

(defgeneric bash-left (object map))
(defgeneric bash-right (object map))
(defgeneric bash-up (object map))
(defgeneric bash-down (object map))

(defmethod bash-left ((object player-character) map)
  (let* ((location (1- (slot-value object 'location)))
         (target (gethash location (game-map-mobiles map))))
    (when (and target (typep target 'mobile))
      (decf (slot-value target 'health) (slot-value object 'melee-attack-damage)))))

(defmethod bash-right ((object player-character) map)
  (let* ((location (1+ (slot-value object 'location)))
         (target (gethash location (game-map-mobiles map))))
    (when (and target (typep target 'mobile))
      (decf (slot-value target 'health) (slot-value object 'melee-attack-damage)))))

(defmethod bash-up ((object player-character) map)
  (let* ((location (- (slot-value object 'location) (game-map-width map)))
         (target (gethash location (game-map-mobiles map))))
    (when (and target (typep target 'mobile))
      (decf (slot-value target 'health) (slot-value object 'melee-attack-damage)))))

(defmethod bash-down ((object player-character) map)
  (let* ((location (+ (slot-value object 'location) (game-map-width map)))
         (target (gethash location (game-map-mobiles map))))
    (when (and target (typep target 'mobile))
      (decf (slot-value target 'health) (slot-value object 'melee-attack-damage)))))

(defun number-to-unicode (number)
  (when (< number 10)
    (let ((mapping #(#x30 #x31 #x32 #x33 #x34 #x35 #x36 #x37 #x38 #x39)))
      (aref mapping number))))

(defun write-map-to-terminal (player map viewport)
  (when (get-location player)
    (let* ((player-location (slot-value player 'location))
           (current-room (find-room player-location map))
           (vision-range (if current-room
                             (with-slots ((x1 crawler::x1) (y1 crawler::y1) (x2 crawler::x2) (y2 crawler::y2)) current-room
                               (list (list (1- x1) (1- y1))
                                     (list x2 y2)))
                             (progn
                               (multiple-value-bind (x y) (calc-coord player-location map)
                                 (list (list (1- x) (1- y))
                                       (list (1+ x) (1+ y))))))))
      (labels ((write-cell (key tile)
                 (multiple-value-bind (x y) (calc-coord (- key (slot-value viewport 'location)) map)
                   (let ((x1 (caar vision-range))
                         (x2 (caadr vision-range))
                         (y1 (cadar vision-range))
                         (y2 (cadadr vision-range)))
                     (when (and (>= x x1)
                                (<= x x2)
                                (>= y y1)
                                (<= y y2))
                       (terminal-color (slot-value tile 'color))
                       (terminal-put x y (slot-value tile 'codepoint)))))))
        (maphash #'write-cell (game-map-terrain map))
        (maphash #'write-cell (game-map-mobiles map))))))

(defun adjacent-open-tiles (tile map)
  (when tile
    (let* ((mobile-neighbors (mapcar #'get-location (get-neighbors tile map (game-map-mobiles map))))
           (terrain-neighbors (mapcar #'get-location (get-neighbors tile map (game-map-terrain map) :filter-fun #'walkablep))))
      (mapcar (lambda (item) (gethash item (game-map-terrain map)))
              (remove-if (lambda (item) (find item mobile-neighbors))
                         terrain-neighbors)))))

;; TODO: Update to be able to handle any distance, not just 1
;; TODO: Update to include `tile' for consideration
(defun nearest-open-tile (tile map)
  (declare (type entity tile))
  (first (adjacent-open-tiles tile map)))

(defun in-room-p (location map room)
  (declare (type crawler::dungeon-room room))
  (multiple-value-bind (x y) (calc-coord location map)
    (with-slots ((x1 crawler::x1) (y1 crawler::y1) (x2 crawler::x2) (y2 crawler::y2)) room
      (and (>= x x1)
           (<= x x2)
           (>= y y1)
           (<= y y2)))))

(defun find-room (location map)
  (when location
    (labels ((location-in-room-p (room) (in-room-p location map room)))
      (find-if #'location-in-room-p (slot-value (slot-value map 'dungeon) 'crawler::rooms)))))

(defun find-arc (x y x0 y0 map)
  (labels ((calc-location-map (x y) (calc-location x y map)))
    (let ((coords (list (list (+ x x0) (+ y y0))
                        (list (+ y x0) (+ x y0))
                        (list (- x0 x) (+ y y0))
                        (list (- x0 y) (+ x y0))
                        (list (- x0 x) (- y0 y))
                        (list (- x0 y) (- y0 x))
                        (list (+ x x0) (- y0 y))
                        (list (+ y x0) (- y0 x)))))
      (loop for i in coords collect (apply #'calc-location-map i)))))

(defun find-circle (location radius map)
  (multiple-value-bind (x0 y0) (calc-coord location map)
    (let ((cur-radius radius)
          (y 0)
          (decision-over-2 (- 1 radius)))
      (loop while (<= y cur-radius)
            as coords = (find-arc cur-radius y x0 y0 map) do
              (incf y)
              (if (<= decision-over-2 0)
                  (incf decision-over-2 (1+ (* 2 y)))
                  (progn
                    (decf cur-radius)
                    (incf decision-over-2 (1+ (* 2 (- y cur-radius))))))
            collect coords))))

;; trivial-ortho-line and ortho-line written by vydd
;; https://github.com/vydd/
;; Ported from https://github.com/SquidPony/SquidLib/blob/master/squidlib-util/src/main/java/squidpony/squidmath/OrthoLine.java
(defun trivial-ortho-line (start-x start-y end-x end-y)
  (let ((start-x (min start-x end-x))
        (end-x (max start-x end-x))
        (start-y (min start-y end-y))
        (end-y (max start-y end-y)))
    (cond ((and (= start-x end-x) (= start-y end-y)) `((,start-x ,start-y)))
          ((= start-x end-x) (loop for y from start-y upto end-y
                                   collect `(,start-x ,y)))
          ((= start-y end-y) (loop for x from start-x upto end-x
                                   collect `(,x ,start-y))))))

(defun ortho-line (start-x start-y end-x end-y)
  (if (or (= start-x end-x)
          (= start-y end-y))
      (trivial-ortho-line start-x start-y end-x end-y)
      (let* ((dx (- end-x start-x))
             (dy (- end-y start-y))
             (nx (abs dx))
             (ny (abs dy))
             (sign-x (if (> dx 0) 1 -1))
             (sign-y (if (> dy 0) 1 -1))
             (work-x start-x)
             (work-y start-y)
             (drawn `((,start-x ,start-y))))
        (loop
          with ix = 0
          with iy = 0
          while (or (< ix nx) (< iy ny))
          do (progn
               (if (< (/ (+ 0.5 ix) nx) (/ (+ 0.5 iy) ny))
                   (setf work-x (+ work-x sign-x)
                         ix (1+ ix))
                   (setf work-y (+ work-y sign-y)
                         iy (1+ iy)))
               (push `(,work-x ,work-y) drawn)))
        (nreverse drawn))))

(defun fill-map-from-crawler (map player)
  (with-slots (width height dungeon mobiles terrain) map
    (dotimes (x width)
      (dotimes (y height)
        (let ((tile (aref (crawler:tile-map dungeon) x y)))
          (cond
            ((not (crawler:walkablep tile))
             (let ((wall (make-instance 'wall)))
               (set-location wall x y map)
               (setf (gethash (calc-location x y map) terrain) wall)))
            ((eq (crawler:map-feature tile) :stairs-up)
             (set-location player x y map)
             (setf (gethash (calc-location x y map) mobiles) player)
             (let ((floor (make-instance 'floor-tile)))
               (set-location floor x y map)
               (setf (gethash (calc-location x y map) terrain) floor)))
            (:otherwise
             (let ((floor (make-instance 'floor-tile)))
               (set-location floor x y map)
               (setf (gethash (calc-location x y map) terrain) floor)))))))))

(defun room-center (room map)
  (with-slots ((x1 crawler::x1) (x2 crawler::x2) (y1 crawler::y1) (y2 crawler::y2)) room
    (calc-location (floor (+ x2 x1) 2) (floor (+ y2 y1) 2) map)))

(defun spawn-enemies (map player)
  (loop for room in (crawler::rooms (game-map-dungeon map))
        when (not (equal room (find-room (get-location player) map))) do
          (dotimes (times (random 3))
            (let* ((location (get-location (nearest-open-tile (gethash (room-center room map) (game-map-terrain map)) map)))
                   (enemy (make-instance 'enemy :location location)))
              (when location
                (setf (gethash location (game-map-mobiles map)) enemy))))))

(defun spawn-tombs (map)
  (loop for room in (crawler::rooms (game-map-dungeon map)) do
    (when (= 2 (random 3))
      (let* ((location (get-location (nearest-open-tile (gethash (room-center room map) (game-map-terrain map)) map)))
             (tomb (make-instance 'tomb :location location)))
        (when location
          (setf (gethash location (game-map-terrain map)) tomb))))))

(defun initialize-map (map player)
  (fill-map-from-crawler map player)
  (update-location player map nil (get-location player))
  (setf (gethash (slot-value player 'location) (game-map-mobiles map)) player)
  (spawn-enemies map player)
  (spawn-tombs map)
  (make-dijkstra-map
   (remove-if-not #'enemy-p
                  (alexandria:hash-table-values (game-map-mobiles map)))
   'enemy-map-value (game-map-terrain map) map)
  map)

(defun get-neighbors (tile map layer &key (filter-fun #'identity))
  (let* ((location (slot-value tile 'location))
         (n (gethash (- location (game-map-width map)) layer))
         (w (gethash (1- location) layer))
         (e (gethash (1+ location) layer))
         (s (gethash (+ location (game-map-width map)) layer)))
    (remove-if-not filter-fun (list w n e s))))

(defun walkablep (tile)
  (and tile (or (typep tile 'floor-tile) (typep tile 'tomb))))

(defun make-dijkstra-map (sources slot tiles map)
  (labels ((get-slot (tile)
             (slot-value tile slot))
           ((setf get-slot) (new-value tile)
             (setf (slot-value tile slot) new-value))
           (set-slot-nil (tile)
             (setf (get-slot tile) nil)))
    ;; Set all walkable tiles to nil
    (mapc #'set-slot-nil (remove-if-not #'walkablep (alexandria:hash-table-values tiles)))
    (let ((pending (make-instance 'cl-heap:priority-queue)))
      ;; Insert all source floor tiles in queue with priority and slot value zero
      (mapc (lambda (item)
              (with-slots (location) item
                (when location
                  (setf (get-slot (gethash location tiles)) 0)
                  (cl-heap:enqueue pending (gethash location tiles) 0)))) sources)
      (loop as tile = (cl-heap:dequeue pending)
            while tile do
              (loop with value = (1+ (get-slot tile))
                    for neighbor in (get-neighbors tile map tiles :filter-fun #'walkablep)
                    when (not (get-slot neighbor)) do
                      (setf (get-slot neighbor) value)
                      (cl-heap:enqueue pending neighbor (get-slot neighbor))))
      tiles)))

(defun create-viewport (map)
  (multiple-value-bind (width height) (get-window-size)
    (when (> width (game-map-width map))
      (setf width (game-map-width map)))
    (when (> height (game-map-height map))
      (setf height (game-map-height map)))
    (make-instance 'viewport :width width :height height)))

(defun find-tomb-rooms-with-tomb (map)
  (labels ((tombp (tile) (typep tile 'tomb)))
    (let ((tombs (remove-if-not #'tombp (alexandria:hash-table-values (game-map-terrain map)))))
      (loop for tomb in tombs
            as room = (find-room (get-location tomb) map)
            as pair = (list tomb room)
            collect pair))))

(defun spawn-minion (tomb map)
  (let* ((location (get-location (nearest-open-tile tomb map)))
         (minion (make-instance 'minion :location location)))
    (setf (gethash location (game-map-mobiles map)) minion)))

(defun wake-the-dead (map player)
  (when (get-location player)
    (let ((player-room (find-room (get-location player) map))
          (tomb-rooms (find-tomb-rooms-with-tomb map)))
      (loop for tomb-room in tomb-rooms
            when (and player-room
                      tomb-room
                      (equal player-room (cadr tomb-room))
                      (not (awakened (car tomb-room)))) do
                        (let ((tomb (car tomb-room)))
                          (setf (awakened tomb) t)
                          (spawn-minion tomb map))))))

(defun end-of-turn-tasks (player map)
  (maphash (lambda (location object)
             (when (typep object 'mobile)
               (if (>= 0 (slot-value object 'health))
                   (progn
                     (setf (slot-value object 'location) nil)
                     (remhash location (game-map-mobiles map))
                     (when (enemy-p object)
                       (make-dijkstra-map
                        (remove-if-not #'enemy-p
                                       (alexandria:hash-table-values (game-map-mobiles map)))
                        'enemy-map-value (game-map-terrain map) map)))
                   (end-of-turn object)))) (game-map-mobiles map))
  (wake-the-dead map player))

(defun write-status-to-terminal (player prev-key map)
  (multiple-value-bind (width height) (get-window-size)
    (declare (ignore width))
    (multiple-value-bind (x y) (calc-coord (get-location player) map)
      (let ((attack-string (cond
                             ((= prev-key bearlibterminal-ffi:+tk-r+) "Choose ranged attack direction or press enter.")
                             ((= prev-key bearlibterminal-ffi:+tk-m+) "Choose melee attack direction or press enter.")
                             (:otherwise ""))))
        (terminal-color (color-from-name "white"))
        (terminal-print 0 (1- height) (format nil "Health: ~d Mana: ~d Location: (~d, ~d) ~A"
                                              (slot-value player 'health)
                                              (slot-value player 'mana)
                                              x y attack-string))))))

(defun ally-p (mobile)
  (typep mobile 'minion))

(defun bash (basher bashee)
  (decf (slot-value bashee 'health) (slot-value basher 'melee-attack-damage)))

(defun can-see-player-p (mobile map player)
  (multiple-value-bind (mob-x mob-y) (calc-coord (get-location mobile) map)
    (multiple-value-bind (player-x player-y) (calc-coord (get-location player) map)
      (or (equal (find-room (get-location mobile) map)
                 (find-room (get-location player) map))
          (let ((line (ortho-line mob-x mob-y player-x player-y))
                (max (get-vision-range mobile)))
            (loop with counter = 0
                  as current = (pop line)
                  while (and current (< counter max))
                  as cur-loc = (calc-location (first current) (cadr current) map)
                  as terrain = (gethash cur-loc (game-map-terrain map))
                  as mobile  = (gethash cur-loc (game-map-mobiles map))
                  while (and (or (not terrain) (not (get-blocks-light terrain)))
                             (or (not mobile)  (not (get-blocks-light mobile))))
                  finally (return (equal player mobile))))))))

(defun move-toward-enemy (mobile map)
  (let* ((tiles (adjacent-open-tiles mobile map))
         (best-tile (first (sort tiles #'< :key #'get-enemy-map-value))))
    (when best-tile
      (remhash (get-location mobile) (game-map-mobiles map))
      (setf (slot-value mobile 'location) (get-location best-tile))
      (setf (gethash (get-location best-tile) (game-map-mobiles map)) mobile))))

(defun distance-to-player (mobile map player &key (max 100))
  (let* ((terrain (game-map-terrain map))
         (tile (gethash (get-location mobile) terrain))
         (player-tile (gethash (get-location player) terrain)))
    (loop with counter = 0
          while (and (not (equal tile player-tile)) (< counter max)) do
            (incf counter)
            (setf tile (first (sort (adjacent-open-tiles tile map) #'< :key #'get-player-map-value)))
          finally (return counter))))

(defun move-ally-toward-player (mobile map player)
  (when (< 3 (distance-to-player mobile map player :max 3))
    (move-toward-player mobile map)))

(defun move-toward-player (mobile map)
  (let* ((tiles (adjacent-open-tiles mobile map))
         (best-tile (first (sort tiles #'< :key #'get-player-map-value))))
    (when best-tile
      (update-location mobile map (get-location mobile) (get-location best-tile)))))

(defun adjacent-enemies (mobile map)
  (get-neighbors mobile map (game-map-mobiles map) :filter-fun #'enemy-p))

(defun adjacent-allies-player (mobile map)
  (get-neighbors mobile map (game-map-mobiles map) :filter-fun (lambda (mobile) (or (typep mobile 'player-character) (typep mobile 'minion)))))

(defun ally-think-and-do (mobile player map)
  (declare (type minion mobile))
  (let ((adjacent-enemy (first (adjacent-enemies mobile map))))
    (cond
      (adjacent-enemy (bash mobile adjacent-enemy))
      ((can-see-player-p mobile map player) (move-toward-enemy mobile map))
      (:otherwise (move-ally-toward-player mobile map player)))))

(defun enemy-think-and-do (mobile player map)
  (declare (type enemy mobile))
  (let ((adjacent-enemy (first (adjacent-allies-player mobile map))))
    (cond
      (adjacent-enemy (bash mobile adjacent-enemy))
      ((can-see-player-p mobile map player) (move-toward-player mobile map)))))

(defun allies-do-stuff (map player)
  (labels ((curried-ally-think-and-do (mobile) (ally-think-and-do mobile player map)))
    (mapc #'curried-ally-think-and-do (remove-if-not #'ally-p (alexandria:hash-table-values (game-map-mobiles map))))))

(defun enemies-do-stuff (map player)
  (labels ((curried-enemy-think-and-do (mobile) (enemy-think-and-do mobile player map)))
    (mapc #'curried-enemy-think-and-do (remove-if-not #'enemy-p (alexandria:hash-table-values (game-map-mobiles map))))))

(defun ai-do-stuff (map player)
  (allies-do-stuff map player)
  (enemies-do-stuff map player))

(defun enemy-p (thing)
  (typep thing 'enemy))

(defun no-enemies-left-p (map)
  (not (find-if #'enemy-p (alexandria:hash-table-values (game-map-mobiles map)))))

(defun display-lose ()
  (terminal-clear)
  (terminal-color (color-from-name "white"))
  (terminal-print 0 0 "You lose!")
  (terminal-print 0 1 "Press escape key to exit.")
  (terminal-refresh)
  (loop as key = (terminal-read)
        while (not (eq key bearlibterminal-ffi:+tk-escape+))))

(defun display-win ()
  (terminal-clear)
  (terminal-color (color-from-name "white"))
  (terminal-print 0 0 "You win!")
  (terminal-print 0 1 "Press escape key to exit.")
  (terminal-refresh)
  (loop as key = (terminal-read)
        while (not (eq key bearlibterminal-ffi:+tk-escape+))))


(defun display-instructions ()
  (terminal-clear)
  (terminal-color (color-from-name "white"))
  (terminal-print 0 0 "Instructions")
  (terminal-print 0 2 "This is you: ")
  (terminal-color (color-from-name "#800080"))
  (terminal-put 13 2 #x2639)
  (terminal-color (color-from-name "white"))
  (terminal-print 0 3 "Move with arrow keys.")
  (terminal-print 0 4 "r is ranged attack. Choose a direction, enter to cancel. Canceling takes your turn. Costs 5 mana.")
  (terminal-print 0 5 "m is melee attack. Choose a direction, enter to cancel. Canceling takes your turn.")
  (terminal-print 0 6 "You do not regenerate health.")
  (terminal-print 0 7 "You regenerate 3 mana per a turn (including turns in which you use mana)")
  (terminal-print 0 8 "Press escape to quit.")
  (terminal-print 0 10 "Tombs: ")
  (terminal-color (color-from-name "#ffff00"))
  (terminal-put 7 10 #x2616)
  (terminal-color (color-from-name "white"))
  (terminal-print 9 10 "Enter a room with a tomb to spawn a minion.")
  (terminal-print 0 11 "Minion: ")
  (terminal-color (color-from-name "#ffff00"))
  (terminal-put 7 11 #x2639)
  (terminal-color (color-from-name "white"))
  (terminal-print 9 11 "Follows you(kind of, WIP) and attacks nearby enemies.")
  (terminal-print 0 12 "Enemy: ")
  (terminal-color (color-from-name "#1919ff"))
  (terminal-put 7 12 #x263a)
  (terminal-color (color-from-name "white"))
  (terminal-print 9 12 "Destroy them!")
  (terminal-print 0 14 "Press enter to continue the game.")
  (terminal-refresh))

(defun intro-message ()
  (multiple-value-bind (win-width win-height) (get-window-size)
    (terminal-print (floor (- (/ win-width 2) 10)) (floor (1- (/ win-height 2))) "Crypts and Corpses")
    (terminal-print (floor (- (/ win-width 2) 11)) (floor (1+ (/ win-height 2))) "Press enter to start"))
  (terminal-refresh)
  (loop as key = (terminal-read)
        while (not (eq key bearlibterminal-ffi:+tk-enter+))))

(defun handle-input (key map viewport player prev-key)
  (cond
    ;; Ranged attack
    ((equal prev-key bearlibterminal-ffi:+tk-r+)
     (cond
       ((= key bearlibterminal-ffi:+tk-up+) (shoot-up player map))
       ((= key bearlibterminal-ffi:+tk-down+) (shoot-down player map))
       ((= key bearlibterminal-ffi:+tk-right+) (shoot-right player map))
       ((= key bearlibterminal-ffi:+tk-left+) (shoot-left player map))
       ((= key bearlibterminal-ffi:+tk-enter+) t))) ; cancel
    ;; Melee attack
    ((equal prev-key bearlibterminal-ffi:+tk-m+)
     (cond
       ((= key bearlibterminal-ffi:+tk-up+) (bash-up player map))
       ((= key bearlibterminal-ffi:+tk-down+) (bash-down player map))
       ((= key bearlibterminal-ffi:+tk-right+) (bash-right player map))
       ((= key bearlibterminal-ffi:+tk-left+) (bash-left player map))
       ((= key bearlibterminal-ffi:+tk-enter+) t))) ; cancel
    ;; Not a stateful command
    (:otherwise
     (cond
       ((= key bearlibterminal-ffi:+tk-r+) t) ; Prepare for ranged
       ((= key bearlibterminal-ffi:+tk-m+) t) ; Prepare for melee
       ((= key bearlibterminal-ffi:+tk-enter+) t) ; idle
       ((= key bearlibterminal-ffi:+tk-h+) (display-instructions))
       ((= key bearlibterminal-ffi:+tk-up+) (move-up player map))
       ((= key bearlibterminal-ffi:+tk-down+) (move-down player map))
       ((= key bearlibterminal-ffi:+tk-right+) (move-right player map))
       ((= key bearlibterminal-ffi:+tk-left+) (move-left player map))
       ((= key bearlibterminal-ffi:+tk-w+) (move-up viewport map))
       ((= key bearlibterminal-ffi:+tk-a+) (move-left viewport map))
       ((= key bearlibterminal-ffi:+tk-d+) (move-right viewport map))
       ((= key bearlibterminal-ffi:+tk-s+) (move-down viewport map))
       ((= key bearlibterminal-ffi:+tk-space+) (update-location viewport map (slot-value viewport 'location) (slot-value player 'location)))))))

(defun main-loop ()
  (loop with player = (make-instance 'player-character)
        with map = (initialize-map (make-instance 'game-map) player)
        with viewport = (create-viewport map)
        with prev-key = nil
        as key = (terminal-read) do
          (when (handle-input key map viewport player prev-key)
            (setf prev-key key)
            (unless (or (= key bearlibterminal-ffi:+tk-r+)
                        (= key bearlibterminal-ffi:+tk-m+))
              (when (get-location player) (ai-do-stuff map player))
              (end-of-turn-tasks player map))
            (terminal-clear)
            (write-map-to-terminal player map viewport)
            (write-status-to-terminal player prev-key map)
            (terminal-refresh))
        until (or (= key bearlibterminal-ffi:+tk-close+)
                  (= key bearlibterminal-ffi:+tk-escape+)
                  (eq (slot-value player 'location) nil)
                  (no-enemies-left-p map))
        finally (cond
                  ((eq (slot-value player 'location) nil) (display-lose))
                  ((no-enemies-left-p map) (display-win)))))

(defun start ()
  (terminal-open)
  (terminal-set "window: size=99x40, title='Crypts and Corpses'")
  (terminal-set (format nil "font: ~SDejaVuSansMono.ttf, size=16"
                        (namestring (asdf:system-relative-pathname :crypts-and-corpses "fonts/"))))
  (intro-message)
  (display-instructions)
  (main-loop)
  (terminal-close))
